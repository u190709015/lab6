public class Circle {

    int radius;
    Point center;

    public Circle(int radius, Point center ) {
        this.radius = radius;
        this.center = center;
    }

    public double area() {
        return Math.PI * this.radius * this.radius ;
    }

    public double perimeter() {
        return 2 * Math.PI * this.radius;
    }

    public boolean intersect(Circle c) {
        double distance  = Math.sqrt(Math.pow(center.xCoord - c.center.xCoord, 2) + Math.pow(center.yCoord - c.center.yCoord, 2));
        return distance < c.radius +radius;
    }

}
