    public class Main {

    public static void main(String[] args) {

        Rectangle r=new Rectangle(8,3,new Point(6,8));

        System.out.println("Area of the rectangle: "+r.area());
        System.out.println("Perimeter of the rectangle: "+r.perimeter());

        Point[] p=r.corners();

        System.out.println("Corners of the rectangle: ");

        for (int i=0;i<p.length;i++) {
            System.out.println(p[i].xCoord+" "+p[i].yCoord);
        }

        Circle c1 = new Circle(10,new Point(5,5));

        System.out.println("Area of the circle: "+c1.area());
        System.out.println("Perimeter of the circle: "+c1.perimeter());

        Circle c2 = new Circle(5,new Point(3,8));

        System.out.println("Intersect(true) or not(false): " + c1.intersect(c2));

    }

}
